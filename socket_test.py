#!/usr/bin/python3.6
import socket
import subprocess
import sys
import confparse
import os
import time
import getpass



def getstamp():
	now = time.localtime(time.time())
	timestamp = str(now.tm_year)+"-"+str(now.tm_mon)+"-"+str(now.tm_mday)+"-"+str(now.tm_hour)+"-"+str(now.tm_min)+"-"+str(now.tm_sec)
	return timestamp


s=""
USER = getpass.getuser()
INSTALL_PATH = '/home/'+USER+'/agent/'
FNULL = open(os.devnull, 'a+')
SCANLIST = open(INSTALL_PATH+'scanexecs', 'w+')
CVELIST = open(INSTALL_PATH+getstamp()+'cve.csv','w')









def socket_init(port): #socket initialitaion and listening to predefined port.
	try:
		s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		print("socket successfully created")
	except socket.error as err:
		print("Failed")
	host = socket.gethostname()

	s.bind(('127.0.0.1',int(port)))
	print("binded")

	s.listen(5)
	print("listening...")
	return s
	# while True:
	# 	c,addr = s.accept()
	# 	if (addr[0] == '127.0.0.1'): #address will be user define and fetched from configuration file 
	# 		print("connection detected from" ,addr)
	# 		msg="Normal Text"
	# 		c.send(msg.encode('ascii'))
	# 		d=c.recv(2048)
	# 		if(d.decode() == "on"):
	# 			print("turning on")
	# 		c.close()
	# 	else:
	# 		print("Not a valid Connection")

	# try:
	# 	hostname=socket.gethostbyname("www.google.com")
	# except socket.gaierror:
	# 	print("Error")
	# 	sys.exit()

REMOTE_SERVER = "www.google.com"
def is_connected():
  try:
    host = socket.gethostbyname(REMOTE_SERVER)
    s = socket.create_connection((host, 80), 2)
    return True
  except:
     pass
  return False



def installcvechecker():
	if is_connected():
		exit_stat=subprocess.call(["yum","install","-y","cvechecker"],stderr=FNULL)#change err to log and output to null
		if exit_stat:
			print("cvechecker successfully installed")
		elif exit_stat==1:
			print("check error log")

		


def iscvecinstall():
	exit_stat=subprocess.call(["cvechecker"],stdout=FNULL)
	if exit_stat == 1:
		return True
	else:
		return False





def generate_cves():
	exit_stat = subprocess.call(["cvechecker","-i"])
	if exit_stat:
		print('SQLit DB initialized')
		print('Pulling NVD CVE Database...(it will take time)')
		exit_stat = subprocess.call(["pullcves","pull"])
		if exit_stat:
			print('finished')
			exit_stat = subprocess.call(["find","/","-type","f","-perm","-o+x"],stdout=SCANLIST)
			if exit_stat == 0 or exit_stat == 1:
				subprocess.call(["echo","/proc/version"], stdout = SCANLIST)
				exit_stat = subprocess.call(["cvechecker","-b"],stdout= SCANLIST)
				if exit_stat:
					exit_stat = subprocess.call(["cvechecker","-r","-C"],stdout=CVELIST)
				else:
					print('cve gen failed')
			else:
				print('finding execs failed')
		else:
			print('NVD DB pulling failed!')
	else:
		print('initialitaion process failed')







def Driver(s,Maddr):
	#print(Maddr)
	while True:
		c,addr = s.accept()
		if (addr[0] == Maddr): #address will be user define and fetched from configuration file 
			print("connection detected from" ,addr[0])
			msg="Normal Text"
			c.send(msg.encode('ascii'))
			d=c.recv(2048)
			if(d.decode() == "genfirst"):
				installcvechecker() if not iscvecinstall() else print("installing..")
				generate_cves()

				#time.sleep(5)
			c.close()
		else:
			socket.close()
			print("Not a valid Connection")
		





if __name__=="__main__":
	addr,port = confparse.ConfInit()
	print(addr)
	s=socket_init(port)
	Driver(s,addr)







##----Main----##
#socket_init()
